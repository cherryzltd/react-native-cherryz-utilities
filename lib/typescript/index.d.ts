declare type CherryzUtilitiesType = {
    testMethod(): void;
    brazeRegisterForPush(): void;
    promptForReview(): void;
};
declare const _default: CherryzUtilitiesType;
export default _default;
