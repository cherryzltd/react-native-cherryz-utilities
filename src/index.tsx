
import { NativeModules } from 'react-native';

const { RNCherryzUtilities } = NativeModules;

type CherryzUtilitiesType = {
  testMethod(): void;
  brazeRegisterForPush(): void;
  promptForReview(): void;
};

export default RNCherryzUtilities as CherryzUtilitiesType;

