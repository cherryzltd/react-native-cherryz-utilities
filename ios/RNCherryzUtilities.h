
#if __has_include("RCTBridgeModule.h")
#import "RCTBridgeModule.h"
#else
#import <React/RCTBridgeModule.h>
#endif
#import <UserNotifications/UserNotifications.h>

@interface RNCherryzUtilities : NSObject <RCTBridgeModule, UNUserNotificationCenterDelegate>

@end
  
