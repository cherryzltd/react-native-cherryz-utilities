
#import "RNCherryzUtilities.h"

#import <UIKit/UIKit.h>

// Required for StoreKit Review prompt
#import <StoreKit/SKStoreReviewController.h>

#import "AppboyKit.h" // deep link support
//#import "AppboyReactUtils.h"

@implementation RNCherryzUtilities

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}
RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(testMethod)
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                    message:@"TestMethod Testiiiing"
                                                   delegate:self
                                          cancelButtonTitle:@"NO"
                                          otherButtonTitles:@"YES", nil];
    
    [alert show];
}

RCT_EXPORT_METHOD(brazeRegisterForPush)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        // Register for user notifications
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_9_x_Max) {
            UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
            center.delegate = self;
            UNAuthorizationOptions options = UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge;

            [center requestAuthorizationWithOptions:options
                                    completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                    [[Appboy sharedInstance] pushAuthorizationFromUserNotificationCenter:granted];
                                    }];
            NSSet *appboyCategories = [ABKPushUtils getAppboyUNNotificationCategorySet];
            [[UNUserNotificationCenter currentNotificationCenter] setNotificationCategories:appboyCategories];
            
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        } else {
            NSSet *appboyCategories = [ABKPushUtils getAppboyUIUserNotificationCategorySet];
            UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeAlert | UIUserNotificationTypeSound) categories:appboyCategories];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        }
    });
}

RCT_EXPORT_METHOD(promptForReview)
{
  if(@available(iOS 10.3, *)) {
    [SKStoreReviewController requestReview];
  }
}

@end
  
