require "json"

package = JSON.parse(File.read(File.join(__dir__, "../package.json")))

Pod::Spec.new do |s|
  s.name         = "RNCherryzUtilities"
  s.version      = package["version"]
  s.summary      = package["description"]
  s.homepage     = package["homepage"]
  s.license      = package["license"]
  s.authors      = package["author"]

  s.platforms    = { :ios => "10.0" }
  s.source       = { :git => "https://bitbucket.org/cherryzltd/react-native-cherryz-utilities/.git", :tag => "#{s.version}" }
  s.framework    = "StoreKit"
  s.source_files = "**/*.{h,m,mm,swift}"

  s.dependency "React-Core"
  s.dependency 'Appboy-iOS-SDK', '4.3.2'
end
