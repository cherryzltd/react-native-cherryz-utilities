# react-native-cherryz-utilities

An assortment of helpful utilities functions used in the Cherryz mobile application.

## Functionality

- Register for native iOS notifications from Braze IAM

```javascript
import RNCherryzUtilities from 'react-native-cherryz-utilities';

// iOS trigger delayed push notification acceptance prompt
RNCherryzUtilities.brazeRegisterForPush();
```

- Present a native review prompt (iOS & Android)

**Note:** Calling this may not trigger the review prompt if certain OS conditions are met - so only call this when you are certain it is a good point of your app flow.

```javascript
import RNCherryzUtilities from 'react-native-cherryz-utilities';

// Show a native review prompt
RNCherryzUtilities.promptForReview();
```

### Automatic installation

1. Add `"git+ssh://git@bitbucket.org/cherryzltd/react-native-cherryz-utilities.git#v1.0.5` to `package.json`
2. `npm i`
3. For iOS, also run `cd ios && pod install`

### Manual installation

#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-cherryz-utilities` and add `RNCherryzUtilities.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNCherryzUtilities.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`

- Add `import com.cherryz.utilities.RNCherryzUtilitiesPackage;` to the imports at the top of the file
- Add `new RNCherryzUtilitiesPackage()` to the list returned by the `getPackages()` method

2. Append the following lines to `android/settings.gradle`:
   ```
   include ':react-native-cherryz-utilities'
   project(':react-native-cherryz-utilities').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-cherryz-utilities/android')
   ```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
   ```
     compile project(':react-native-cherryz-utilities')
   ```
