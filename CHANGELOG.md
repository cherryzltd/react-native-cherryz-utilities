# Changelog

## v1.1.4

- Updated Braze to v1.32.x

## v1.1.3

- Updated podspec file to ensure iOS native code is correctly linked.

## v1.1.2

- Added support for distribution via npm.

## v1.1.1

- Updated Braze to v1.30.x

## v1.1.0

- Refactored module to use TypeScript.

## v1.0.9

- Updated Braze to v1.26.0.
- Updated React Native to v0.63.4.

## v1.0.8

- Re-release of v1.0.7, that also includes the Android in-app review flow logic.

## v1.0.7

- Updated Braze to v1.25.0

## v1.0.6

- Updated Braze to v1.24.0

## v1.0.5

Do not use this version, as the review prompts current are not functioning as expected.

- Added native Android code for presenting in-app review prompt & flow.

## v1.0.0

Braze delayed push notification prompt
