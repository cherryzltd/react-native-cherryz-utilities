package com.cherryz.utilities

import android.app.AlertDialog
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod
import com.google.android.play.core.review.ReviewInfo
import com.google.android.play.core.review.ReviewManagerFactory
import com.google.android.play.core.tasks.Task

class CherryzUtilitiesModule(reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext) {

  override fun getName(): String {
    return "RNCherryzUtilities"
  }

  @ReactMethod
  fun testMethod() {
    val mActivity = currentActivity
    val alertDialog = AlertDialog.Builder(mActivity).create()
    alertDialog.setTitle("Alert")
    alertDialog.setMessage("TestMethod Testiiiing")
    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK"
    ) { dialog, _ -> dialog.dismiss() }
    alertDialog.show()
  }

  @ReactMethod
  fun brazeRegisterForPush() {
    //dummy method
  }

  @ReactMethod
  fun promptForReview() {
    val manager = ReviewManagerFactory.create(reactApplicationContext)
    val request = manager.requestReviewFlow()
    request.addOnCompleteListener { task: Task<ReviewInfo?> ->
      if (task.isSuccessful) {
        val reviewInfo = task.result
        val flow = manager.launchReviewFlow(currentActivity, reviewInfo)
        flow.addOnCompleteListener { flowTask: Task<Void?>? -> }
      }
    }
  }
}
